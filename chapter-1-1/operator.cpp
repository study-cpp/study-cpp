#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>

int main(const int argc, const char** argv) {
  int a = 1, b = 2;

  std::cout << a << " + " << b << " = " << ( a + b ) << std::endl;
  std::cout << a << " - " << b << " = " << ( a - b ) << std::endl;
  std::cout << a << " * " << b << " = " << ( a * b ) << std::endl;
  std::cout << a << " / " << b << " = " << ( a / b ) << std::endl;

  // boolean : 참 , 거짓
  if (a > b) {
    std::cout << "a는 b보다 큽니다." << std::endl;
  } else {
    std::cout << "a는 b보다 크지 않습니다." << std::endl;
  }

  std::cout << a << "는 " << b << "보다 " << ( a < b ? "작습니다." : "작지 않습니다." ) << std::endl;
  std::cout << a << "는 " << b << "보다 " << ( a <= b ? "같거나 작습니다." : " 같거나 작지 않습니다." ) << std::endl;
  std::cout << a << "는 " << b << "보다 " << ( a > b ? "큽니다.." : "크지 않습니다." ) << std::endl;
  std::cout << a << "는 " << b << "보다 " << ( a >= b ? "같거나 큽니다." : "같거나 크지 않습니다." ) << std::endl;
  std::cout << a << "는 " << b << "와 " << ( a == b ? "같습니다." : "다릅니다." ) << std::endl;
  std::cout << a << "는 " << b << "와 " << ( a != b ? "다릅니다." : "같습니다." ) << std::endl;
}
