#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

bool isPrimeNumber(int number) {
  for(int i = 2; i < number; ++i) {
    if (number % i == 0) {
      return false;
    }
  }
  return true;
}

int main(const int argc, const char** argv) {
  int number = 5;
  if (argc > 1) {
    number = atoi(argv[1]);
  }

  cout << number << "는 ";
  if (isPrimeNumber(number)) {
    cout << "소수입니다." << endl;
  } else {
    cout << "소수가 아닙니다." << endl;
  }
}
