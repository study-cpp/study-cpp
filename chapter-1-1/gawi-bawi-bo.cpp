#include <cstdlib>
#include <iostream>
#include <ctime>
#include <string>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

int getRandomNumber(int min, int max) {
  int tempMin = std::min(min, max);
  int tempMax = std::max(min, max);
  if (tempMin == tempMax) {
    return tempMin;
  }

  int randomNumber = rand();
  return ( abs(randomNumber) + tempMin ) % tempMax;
}

string getWordByNumber(int input) {
  const int GAWI = 0, BAWI = 1, BO = 2;
  switch(input) {
    case GAWI: return "가위";
    case BAWI: return "바위";
    case BO: return "보";
    default: return "알 수 없는 값";
  }
}

int main(const int argc, const char** argv) {
  srand(time(0));
  int targetNumber = getRandomNumber(0, 2), input;
  while(true) {
    cout << "가위 바위 보를 입력하세요. [ 0 = 가위 , 1 = 바위 , 2 = 보 ] : ";
    cin >> input;

    if (input < 0 || input > 2) {
      cout << "0 = 가위 , 1 = 바위 , 2 = 보 입니다. 다시 입력해주세요." << endl;
      continue;
    }

    if (input != targetNumber) {
      if ((input + 1) % 3 == targetNumber) {
        cout << "컴퓨터가 " << getWordByNumber(targetNumber) << "를 내서 졌습니다." << endl;
      } else {
        cout << "컴퓨터가 " << getWordByNumber(targetNumber) << "를 내서 이겼습니다." << endl;
      }
      break;
    }

    cout << "컴퓨터가 " << getWordByNumber(targetNumber) << "를 내서 비겼습니다. 다시 합니다." << endl;
    targetNumber = getRandomNumber(0, 2);
  }
}
