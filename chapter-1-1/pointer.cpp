#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

int main(const int argc, const char** argv) {
  int value = 5;
  int* valuePointer1 = &value;
  int* valuePointer2 = &value;
  int** valuePointerPointer = &valuePointer1;

  cout << "value : " << value << endl;
  cout << "value address : " << &value << endl << endl;

  cout << "value pointer 1 : " << valuePointer1 << endl;
  cout << "value pointer 1 address : " << &valuePointer1 << endl;
  cout << "value pointer 1 value : " << *valuePointer1 << endl << endl;

  cout << "value pointer 2 : " << &valuePointer2 << endl;
  cout << "value pointer 2 address : " << &valuePointer2 << endl;
  cout << "value pointer 2 value : " << *valuePointer2 << endl << endl;

  cout << "value pointer pointer address : " << valuePointerPointer << endl;
  cout << "value pointer pointer value : " << *valuePointerPointer << endl;
  cout << "value pointer value : " << **valuePointerPointer << endl << endl;

  *valuePointer1 = 6;
  cout << "value : " << value << endl;

  *valuePointer2 = 7;
  cout << "value : " << value << endl;

  **valuePointerPointer = 8;
  cout << "value : " << value << endl;

  return 0;
}
