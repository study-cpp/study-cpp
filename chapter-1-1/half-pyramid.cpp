#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

string getStarByCount(int count);

int main(const int argc, const char** argv) {
  int limit = 5;
  if (argc > 1) {
    limit = atoi(argv[1]);
  }

  if (limit % 2 < 1) {
    cout << "음수나 짝수는 사용할 수 없습니다." << endl;
    return 1;
  }

  for(int i = 1; i <= limit; ++i) {
    cout << getStarByCount(i) << endl;
  }
  return 0;
}

string getStarByCount(int count) {
  string starts = "";
  for(int i = 0; i < count; ++i) {
    starts += "*";
  }
  return starts;
}
