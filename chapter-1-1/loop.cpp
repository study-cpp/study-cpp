#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>

int main(const int argc, const char** argv) {
  const int start = 1, limit = 10;
  int i = start, total = 0;

  while(i <= limit) {
    total += i; // equal this , total = total + i;
    ++i; // i++; equal this , i = i + 1;
  }
  std::cout << start << "부터 " << limit << "까지의 합은 " << total << "입니다." << std::endl;

  total = 0;
  // i = start;
  for (i = start; i <= limit; ++i) {
    total += i;
    // ++i;
  }
  std::cout << start << "부터 " << limit << "까지의 합은 " << total << "입니다." << std::endl;

  std::cout << std::endl;

  std::cout << "post operator result" << std::endl;
  i = 0;
  while(i < limit) {
    std::cout << i++ << std::endl;
  }
  std::cout << std::endl;

  std::cout << "pre operator result" << std::endl;
  i = 0;
  while(i < limit) {
    std::cout << ++i << std::endl;
  }
}
