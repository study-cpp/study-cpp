#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

string getStar(int count, int rowCount);

int main(const int argc, const char** argv) {
  int limit = 5;
  if (argc > 1) {
    limit = atoi(argv[1]);
  }

  if (limit % 2 < 1) {
    cout << "음수나 짝수는 사용할 수 없습니다." << endl;
    return 1;
  }

  for(int i = 1; i <= limit; ++i) {
    cout << getStar(i, limit) << endl;
  }
  return 0;
}

string getStar(int count, int rowCount) {
  if (count > (rowCount / 2) + 1) {
    return getStar(rowCount - count + 1, rowCount);
  }

  string starts = "";
  int spaceCount = rowCount - (rowCount / 2) - count;
  for(int i = 0; i < spaceCount; ++i) {
    starts += " ";
  }
  int starCount = count * 2 - 1;
  for(int i = 0; i < starCount; ++i) {
    starts += "*";
  }
  return starts;
}
