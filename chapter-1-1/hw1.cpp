#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>

long add(const long a, const long b);
long inputNumber(const int tryCount);
long addNumber(const int limit, const int sum, const int tryCount);

int main(const int argc, const char** argv) {
  int limit = 5;
  if (argc > 1) {
    limit = std::atoi(argv[1]);
  }
  
  long sum = addNumber(limit, 0, 0);
  std::cout << std::endl << "sum : " << sum << std::endl;
  return 0;
}

long add(const long a, const long b) {
  return a + b;
}

long inputNumber(const int tryCount) {
  std::cout << (tryCount + 1) << "> input number : ";
  long input = 0;
  std::cin >> input;
  return input;
}

long addNumber(const int limit, const int sum, const int tryCount) {
  if (tryCount >= limit) {
    return sum;
  }
  return addNumber(limit, sum + inputNumber(tryCount), tryCount + 1);
}
