#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

int main(const int argc, const char** argv) {
  char diamond[5][6] = {
    { ' ', ' ', '*', ' ', ' ', 0x00 },
    { ' ', '*', '*', '*', ' ', 0x00 },
    { '*', '*', '*', '*', '*', 0x00 },
    { ' ', '*', '*', '*', ' ', 0x00 },
    { ' ', ' ', '*', ' ', ' ', 0x00 },
  };

  for(int i = 0; i < 5; ++i) {
    cout << diamond[i] << endl;
    cout << &diamond[i] << endl;
  }

  cout << endl;

  for(int i = 0; i < 5; ++i) {
    cout << *(diamond + i) << endl;
    cout << (diamond + i) << endl;
  }

  return 0;
}
