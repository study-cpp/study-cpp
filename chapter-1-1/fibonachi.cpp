#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

int main(const int argc, const char** argv) {
  int limit = 5;
  if (argc > 1) {
    limit = atoi(argv[1]);
  }

  int a = 0, b = 0, c = 0;
  for (int i = 0; i < limit; ++i) {
    cout << b << " ";
    c = b;
    b += a;
    a = c;
    if (b < 1) {
      b = 1;
    } else if (a < 1) {
      a = 1;
    }
  }
  cout << endl;

  return 0;
}
