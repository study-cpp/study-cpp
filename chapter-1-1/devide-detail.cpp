#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

int main(const int argc, const char** argv) {
  int dividend = atoi(argv[1]);
  int divisor = atoi(argv[2]);
  if (divisor == 0) {
    cout << "0으로 나눌 수 없습니다." << endl;
    return 1;
  }

  int rq = 0;
  int quotient = 1, remainder = 1;
  while(quotient > 0 && remainder > 0) {
    int temp = divisor;
    int numberOfDigit = 0;
    while(temp < dividend) {
      if (temp * 10 > dividend) {
        break;
      }
      temp *= 10;
      ++numberOfDigit;
    }

    quotient = dividend / temp;
    for(int i = 0; i < numberOfDigit; ++i) {
      quotient *= 10;
    }
    remainder = dividend % temp;

    rq += quotient;

    cout << "몫 = " << rq << " , 나머지 = " << remainder << endl;

    dividend -= quotient * divisor;
  }
}
