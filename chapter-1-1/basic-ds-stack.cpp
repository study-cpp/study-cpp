#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

// stack => FILO => First In, Last Out

namespace stack {
  const int size = 5;
  static int cursor = 0;
  static int store[size] = {};
  bool push(const int value) {
    if (cursor + 1 > size) {
      cout << "stack > overflow , cannot push!" << endl;
      return false;
    }
    store[cursor++] = value;
    return true;
  }
  bool pop(const int* value) {
    if (cursor - 1 < 0) {
      cout << "stack > underflow , cannot pop!" << endl;
      return false;
    }
    *value = store[--cursor];
    return true;
  }
  void dump() {
    cout << "stack dump :";
    for(int i = 0; i < cursor; ++i) {
      if (i > 0) {
        cout << " ,";
      }
      cout << " " << store[i];
    }
    cout << endl;
  }
}

int main(const int argc, const char** argv) {
  int value;

  stack::push(5);
  stack::dump();

  stack::push(4);
  stack::dump();

  stack::push(3);
  stack::dump();

  stack::push(2);
  stack::dump();

  stack::push(1);
  stack::dump();

  stack::push(15);
  // failure here > overflow

  for(int i = 0; i < stack::size; ++i) {
    stack::pop(&value);
    cout << value << endl;
    stack::dump();
  }

  stack::pop(&value);
  // failure here > underflow
  return 0;
}
