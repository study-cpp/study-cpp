#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

int main(const int argc, const char** argv) {
  int digit = atoi(argv[1]);
  int temp = digit;
  int countOfDigit = 1;
  while(temp > 9) {
    ++countOfDigit;
    temp = temp / 10;
  }
  cout << digit << "의 자릿수는 " << countOfDigit << "입니다." << endl;
  return 0;
}
