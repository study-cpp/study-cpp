#include <cstdlib>
#include <iostream>
#include <ctime>
#include <string>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

int getRandomNumber(int min, int max) {
  int tempMin = std::min(min, max);
  int tempMax = std::max(min, max);
  if (tempMin == tempMax) {
    return tempMin;
  }

  srand(time(0));
  int randomNumber = rand();
  return ( abs(randomNumber) + tempMin ) % tempMax;
}

int main(const int argc, const char** argv) {
  int min = 1, max = 100, targetNumber = getRandomNumber(min, max), input;
  while(true) {
    cout << "숫자를 입력하세요 [ " << min << " ~ " << max << " ] > ";
    cin >> input;

    if (input < min || input > max) {
      cout << min << "부터 " << max << "까지의 값만 입력하세요." << endl;
      continue;
    }

    if (input < targetNumber) {
      cout << input << "보다 큽니다." << endl;
      min = input + 1;
    } else if (input > targetNumber) {
      cout << input << "보다 작습니다." << endl;
      max = input - 1;
    } else {
      cout << "축하합니다. 정답은 " << input << "입니다." << endl;
      break;
    }
  }
  return 0;
}
