#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

int main(const int argc, const char** argv) {
  int dividend = atoi(argv[1]);
  int divisor = atoi(argv[2]);
  if (divisor == 0) {
    cout << "0으로 나눌 수 없습니다." << endl;
    return 1;
  }

  while(true) {
    int quotient, remainder;
    quotient = dividend / divisor;
    remainder = dividend % divisor;
    cout << "몫 = " << quotient << " , 나머지 = " << remainder << endl;
    if (quotient == 0 || remainder == 0) {
      break;
    }
    dividend = remainder;
  }
}

