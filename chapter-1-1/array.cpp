#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

namespace util {
  namespace array {
    int sum(const int array[], const int sizeOfArray) {
      int total = 0;
      for(int i = 0; i < sizeOfArray; ++i) {
        total += array[i];
      }
      return total;
    }
  }
}

int arraySum(const int array[], int sizeOfArray) {
  int total = 0;
  for(int i = 0; i < sizeOfArray; ++i) {
    total += array[i];
  }
  return total;
}

int main(const int argc, const char** argv) {
  int arraySize = 5, array[arraySize];
  for(int i = 0; i < arraySize; ++i) {
    array[i] = i + 1;
  }

  // equal this
  // array[0] = 1;
  // array[1] = 2;
  // ...
  // array[4] = 5;

  // and equal this
  // int array[5] = { 1, 2, 3, 4, 5 };

  cout << "1부터 5까지의 합은 " << util::array::sum(array, arraySize) << endl;
  cout << "1부터 5까지의 합은 " << arraySum(array, arraySize) << endl;
  return 0;
}
